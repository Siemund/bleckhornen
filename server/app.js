//Express app
const express = require('express')
const app = express()
const bodyParser = require('body-parser')
const morgan = require('morgan')

const joinRoutes = require('./api/routes/join')
const bookRoutes = require('./api/routes/book')

//Middleware
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

// Handle CORS-problems
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization'
  )
  // Browsers send this before POST-methods
  if (req.method === 'OPTIONS') {
    res.header('Access-Control-Allow-Methods', 'PUT, POST, PACHT, DELETE, GET')
    return res.status(200).json({})
  }
  next()
})

//Routes
app.use('/join', joinRoutes)
app.use('/book', bookRoutes)

//In case of incorrect route
app.use((req, res, next) => {
  const error = new Error('Not found')
  error.status = 404
  next(error)
})

//To handle all errors in the application
app.use((error, req, res, next) => {
  res.status(error.status || 500)
  res.json({
    error: {
      message: error.message
    }
  })
})

module.exports = app
