const express = require('express')
const router = express.Router()
const nodemailer = require('nodemailer')

const transporter = nodemailer.createTransport({
  host: 'smtp.ethereal.email',
  port: 587,
  auth: {
    user: 'ydgpqx7gawxq4cjh@ethereal.email',
    pass: 'yM1qWT9CTBJvHUrZsj'
  }
})

router.post('/', (req, res, next) => {
  let mailOptions = {
    from: '"bleckhornen.org" <info@bleckhornen.org>', // sender address
    to: 'info@bleckhornen.org', // list of receivers
    subject: 'Ny intresseanmälan', // Subject line
    text:
      'Ny bokningsförfrågan: \n\nNamn: ' +
      req.body.firstName +
      ' ' +
      req.body.surname +
      '\nEmail: ' +
      req.body.email +
      '\nTelefonummer: ' +
      req.body.phone +
      '\nDatum: ' +
      req.body.date +
      '\nOm: ' +
      req.body.about
  }

  // send mail with defined transport object
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      res.status(503).json({ msg: 'Message could not be sent' })
    }
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info))
    res.status(200).json({ msg: 'Message sent' })
  })
})

module.exports = router
