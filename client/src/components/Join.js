import React, { Component } from 'react'
import axios from 'axios'

import '../styles/Form.css'
import FormResponse from './FormResponse'
import SendButton from './SendButton'

const alternatives = [
  'Altsaxofon',
  'Balett',
  'Barytonsaxofon',
  'Basklarinett',
  'Euphonium',
  'Fagott',
  'Flöjt',
  'Horn',
  'Klarinett',
  'Lyra',
  'Oboe',
  'Slagverk',
  'Sopransaxofon',
  'Tenorsaxofon',
  'Trombon',
  'Trumpet',
  'Tuba'
]

class Join extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      surname: '',
      email: '',
      phone: '',
      instrument: alternatives[0],
      about: 'Bleckhornen är ju bäst!!',
      returnCode: 0,
      sending: false
    }
  }

  renderDropDown = () => {
    return alternatives.map(event => {
      return (
        <option
          key={event}
          onClick={() => this.setState({ instrument: event })}
        >
          {event}
        </option>
      )
    })
  }

  canSend = () => {
    const contacts = this.state.phone + this.state.email
    const canSend =
      this.state.firstName !== '' &&
      this.state.surname !== '' &&
      contacts !== ''
    return canSend
  }

  onInputChange = event => {
    const name = event.target.name
    this.setState({ [name]: event.target.value })
  }

  onFormSubmit = event => {
    event.preventDefault()
    this.setState({ returnCode: 0 })
    this.setState({ sending: true })
    const toSend = this.state
    delete toSend.returnCode
    axios
      .post('http://localhost:8000/join', toSend)
      .then(response => {
        this.setState({
          firstName: '',
          surname: '',
          email: '',
          phone: '',
          instrument: alternatives[0],
          about: 'Bleckhornen är ju bäst!!',
          returnCode: 1,
          sending: false
        })
        window.scrollTo(0, 0)
      })
      .catch(error => {
        this.setState({ returnCode: 2, sending: false })
        window.scrollTo(0, 0)
      })
  }

  render() {
    return (
      <div
        className="container book-body"
        style={{ paddingTop: '20px', paddingBottom: '20px' }}
      >
        <div className="row">
          <div className="col-lg-8 col-12">
            <h1>Intresseanmälan</h1>
            Vi är alltid intresserade av att få med nya musiker eller dansare
            till vårt glada gäng. Skicka in en intresseanmälan om du är
            intresserad!
            <FormResponse returnCode={this.state.returnCode} />
            {this.props.joinRequest}
            <form onSubmit={this.onFormSubmit}>
              <div className="form-group">
                <label>Förnamn:</label>
                <input
                  name="firstName"
                  type="text"
                  className="form-control"
                  id="exampleFormControlInput1"
                  placeholder="Förnamn"
                  value={this.state.firstName}
                  onChange={this.onInputChange}
                />
                <div className="invalid-feedback">
                  Vänligen fyll i denna ruta innan du skickar.
                </div>
              </div>
              <div className="form-group">
                <label>Efternamn:</label>
                <input
                  name="surname"
                  type="text"
                  className="form-control"
                  id="exampleFormControlInput1"
                  placeholder="Efternamn"
                  value={this.state.surname}
                  onChange={this.onInputChange}
                />
                <div className="invalid-feedback">
                  Vänligen fyll i denna ruta innan du skickar.
                </div>
              </div>
              <div className="form-group">
                <label>E-post:</label>
                <input
                  name="email"
                  type="email"
                  className="form-control"
                  id="exampleFormControlInput1"
                  placeholder="namn@exempel.com"
                  value={this.state.email}
                  onChange={this.onInputChange}
                />
              </div>
              <div className="form-group">
                <label>Mobilnummer:</label>
                <input
                  name="phone"
                  type="tel"
                  className="form-control"
                  id="exampleFormControlInput1"
                  placeholder="0700123456"
                  value={this.state.phone}
                  onChange={this.onInputChange}
                />
              </div>
              <div className="form-group">
                <label>Jag vill spela/dansa i följande sektion:</label>
                <select className="form-control" id="exampleFormControlSelect1">
                  {this.renderDropDown()}
                </select>
              </div>
              <div className="form-group">
                <label>
                  Berätta gärna vem du är och varför du vill vara med:
                </label>
                <textarea
                  name="about"
                  className="form-control"
                  id="exampleFormControlTextarea1"
                  rows="3"
                  value={this.state.about}
                  onChange={this.onInputChange}
                />
              </div>
              <SendButton
                canSend={this.canSend()}
                sending={this.state.sending}
                text="Skicka anmälan"
              />
            </form>
          </div>
          <div className="col-lg-4 col-12 images">
            <img
              src="https://res.cloudinary.com/fredriksiemund/image/upload/v1516801524/Bleckhornen/rom.jpg"
              alt="rom"
            />
            <img
              src="https://res.cloudinary.com/fredriksiemund/image/upload/v1516800610/Bleckhornen/grand.jpg"
              alt="grand"
            />
            <img
              src="https://res.cloudinary.com/fredriksiemund/image/upload/v1516801523/Bleckhornen/julkoncert.jpg"
              alt="julkoncert"
            />
          </div>
        </div>
      </div>
    )
  }
}

export default Join
