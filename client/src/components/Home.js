import React, { Component } from 'react'

import Calendar from '../containers/Calendar'
import News from '../containers/News'
import '../styles/Home.css'

export default class Home extends Component {
  render() {
    return (
      <div className="home-body">
        <div className="container info-part">
          <div className="welcome-box">
            <h1>Välkommen till Bleckhornen!</h1>
            <p className="lead">
              Välkommen till Lunds äldsta och bästa studentorkester!
            </p>
          </div>
          <div className="row">
            <div className="col-lg-7 col-xs-12">
              <News />
            </div>
            <div className="col-lg-5 col-xs-12 calendar-div">
              <Calendar />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
