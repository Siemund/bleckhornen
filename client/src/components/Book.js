import React, { Component } from 'react'
import axios from 'axios'

import '../styles/Form.css'
import FormResponse from './FormResponse'
import SendButton from './SendButton'

export default class Book extends Component {
  constructor(props) {
    super(props)
    this.state = {
      firstName: '',
      surname: '',
      email: '',
      phone: '',
      date: '',
      about: '',
      returnCode: 0,
      sending: false
    }
  }

  onInputChange = event => {
    const name = event.target.name
    this.setState({ [name]: event.target.value })
  }

  onFormSubmit = event => {
    console.log(event.target)
  }

  canSend = () => {
    const contacts = this.state.phone + this.state.email
    const canSend =
      this.state.firstName !== '' &&
      this.state.surname !== '' &&
      this.state.about !== '' &&
      this.state.date !== '' &&
      contacts !== ''
    return canSend
  }

  onFormSubmit = event => {
    event.preventDefault()
    this.setState({ returnCode: 0 })
    this.setState({ sending: true })
    const toSend = this.state
    delete toSend.returnCode
    delete toSend.sending
    axios
      .post('http://localhost:8000/book', toSend)
      .then(response => {
        this.setState({
          firstName: '',
          surname: '',
          email: '',
          phone: '',
          date: '',
          about: '',
          returnCode: 1,
          sending: false
        })
      })
      .catch(error => {
        this.setState({ returnCode: 2, sending: false })
      })
  }

  render() {
    return (
      <div
        className="container book-body"
        style={{ paddingTop: '20px', paddingBottom: '20px' }}
      >
        <div className="row">
          <div className="col-lg-8">
            <h1>Bokningsförfrågan</h1>
            Bleckhornen spelar varsomhelst, närsomhelst, hursomhelst, helst!
            Invigning, möhippa, 50årsfest, konferens, student, eller helt enkelt
            när ni känner för att fira. Allt från jazz och swing till schlager
            och pop spelas, ofta i egna arrangemang. Vi höjer humöret på vår
            publik med vår spelglädje och galna upptåg. Vi kan spela
            stillastående eller gåendes, med notklämmor eller hela pärmar,
            inomhus eller utomhus, ni väljer. Kontakta oss och vi ser till att
            er tillställning blir minnesvärd!
            <FormResponse returnCode={this.state.returnCode} />
            <form onSubmit={this.onFormSubmit}>
              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Förnamn:</label>
                <input
                  name="firstName"
                  type="text"
                  className="form-control"
                  placeholder="Förnamn"
                  value={this.state.firstName}
                  onChange={this.onInputChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Efternamn:</label>
                <input
                  name="surname"
                  type="text"
                  className="form-control"
                  placeholder="Efternamn"
                  value={this.state.surname}
                  onChange={this.onInputChange}
                />
              </div>
              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">E-post:</label>
                <input
                  name="email"
                  type="email"
                  className="form-control"
                  placeholder="namn@exempel.com"
                  value={this.state.email}
                  onChange={this.onInputChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Mobilnummer:</label>
                <input
                  name="phone"
                  type="tel"
                  className="form-control"
                  placeholder="0700123456"
                  value={this.state.phone}
                  onChange={this.onInputChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="exampleFormControlInput1">Datum:</label>
                <input
                  name="date"
                  type="date"
                  className="form-control"
                  placeholder="YYYY-MM-DD"
                  value={this.state.date}
                  onChange={this.onInputChange}
                />
              </div>

              <div className="form-group">
                <label htmlFor="exampleFormControlTextarea1">
                  Berätta lite kort om spelningen:
                </label>
                <textarea
                  name="about"
                  className="form-control"
                  id="exampleFormControlTextarea1"
                  rows="3"
                  value={this.state.about}
                  onChange={this.onInputChange}
                />
              </div>
              <SendButton
                canSend={this.canSend()}
                sending={this.state.sending}
                text="Skicka förfrågan"
              />
            </form>
          </div>
          <div className="col-lg-4 images">
            <img
              src="https://res.cloudinary.com/fredriksiemund/image/upload/v1516798759/Bleckhornen/kulturen.jpg"
              alt="kulturen"
            />
            <img
              src="https://res.cloudinary.com/fredriksiemund/image/upload/v1516801016/Bleckhornen/spelning.jpg"
              alt="spelning"
            />
            <img
              src="https://res.cloudinary.com/fredriksiemund/image/upload/v1516798759/Bleckhornen/taget.jpg"
              alt="tåget"
            />
          </div>
        </div>
      </div>
    )
  }
}
