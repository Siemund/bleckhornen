import React, { Component } from 'react'
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import Header from './Header'
import Navbar from './Navbar'
import About from './About'
import Home from './Home'
import Join from './Join'
import Book from './Book'
import Admin from './AdminPage'
import Footer from './Footer'

class App extends Component {
  render() {
    return (
      <div className="complete">
        <BrowserRouter>
          <div>
            <Header />
            <Navbar />
            <Switch>
              <Route path="/admin" component={Admin} />
              <Route path="/about" component={About} />
              <Route path="/join" component={Join} />
              <Route path="/book" component={Book} />
              <Route path="/" component={Home} />
            </Switch>
            <Footer />
          </div>
        </BrowserRouter>
      </div>
    )
  }
}

export default App
