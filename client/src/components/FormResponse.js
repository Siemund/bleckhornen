import React from 'react'

const success = (
  <div
    className="alert alert-success"
    role="alert"
    style={{ marginBottom: 0, marginTop: 5 }}
  >
    Vi har mottagit ditt meddelande och återkommer så snart som möjligt!
  </div>
)

const failure = (
  <div
    className="alert alert-danger"
    role="alert"
    style={{ marginBottom: 0, marginTop: 5 }}
  >
    Tyvärr gick det inte att skicka meddelandet. Vänligen försök en gång till,
    skulle det återigen inte fungera skicka istället ett meddelande direkt till
    styrelsen@bleckhornen.org
  </div>
)

const FormResponse = result => {
  switch (result.returnCode) {
    case 1:
      return success
    case 2:
      return failure
    default:
      return ''
  }
}

export default FormResponse
