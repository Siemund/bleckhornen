import React, { Component } from 'react'

/* eslint-disable no-undef */
/* global google */
class GoogleMap extends Component {
  componentDidMount() {
    let map = new google.maps.Map(this.refs.map, {
      zoom: 16,
      center: {
        lat: 55.705878,
        lng: 13.19602
      }
    })
    let marker1 = new google.maps.Marker({
      position: {
        lat: 55.705461,
        lng: 13.197592
      },
      map: map
    })
    let marker2 = new google.maps.Marker({
      position: {
        lat: 55.70599,
        lng: 13.194797
      },
      map: map
    })
    new google.maps.InfoWindow({
      content: '<strong>Tarmen</strong><br />Vår föreningslokal!'
    }).open(map, marker1)
    new google.maps.InfoWindow({
      content: '<strong>Palaestra et Odeum</strong><br />Här repar vi!'
    }).open(map, marker2)
  }

  render() {
    return <div ref="map" className="google-map" />
  }
}

export default GoogleMap
