import React from 'react'

const Contact = props => {
  return (
    <tr>
      <td className="text">{props.post}</td>
      <td className="text">
        {props.name + ' '}
        <a href={'mailto:' + props.mail}>
          <i className="fa fa-envelope-o" />
        </a>
      </td>
      <td className="text">{props.phone}</td>
    </tr>
  )
}

export default Contact
