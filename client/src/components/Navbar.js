import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'

const linkStyle = {
  fontSize: '20px',
  marginRight: '30px',
  ouline: 0
}

class Navbar extends Component {
  checkActive(name) {
    const result = 'nav-item nav-link '
    if (this.props.location.pathname === name) {
      return result + 'active'
    }
    return result
  }

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <div className="container">
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarNavAltMarkup"
            aria-controls="navbarNavAltMarkup"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <Link className={this.checkActive('/')} style={linkStyle} to="/">
                Hem
              </Link>
              <Link
                className={this.checkActive('/about')}
                style={linkStyle}
                to="/about"
              >
                Om oss
              </Link>
              <Link
                className={this.checkActive('/book')}
                style={linkStyle}
                to="/book"
              >
                Boka
              </Link>
              <Link
                className={this.checkActive('/join')}
                style={linkStyle}
                to="/join"
              >
                Gå med
              </Link>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default withRouter(Navbar)