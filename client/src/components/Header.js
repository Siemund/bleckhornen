import React, { Component } from 'react'

import '../styles/Header.css'

export default class Header extends Component {
  render() {
    return (
      <header className="clearfix">
        <div className="container">
          <div className="row">
            <div className="col-lg-10 col-12">
              <ul className="list-inline">
                <li className="list-inline-item">
                  <img
                    alt="logo"
                    src="https://res.cloudinary.com/fredriksiemund/image/upload/v1529609436/logo_tchm3n.png"
                    className="logo"
                  />
                </li>
                <li className="list-inline-item title">
                  <h1>AMC Bleckhornen</h1>
                </li>
              </ul>
            </div>

            <div className="col-lg-2 col-12">
              <div className="external">
                <a
                  href="http://www.bleckhornen.org/appendix/index.php"
                  className="btn btn-light"
                >
                  Till Blindtarmen
                </a>
                <br />
                <a href="https://www.facebook.com/amcbleckhornen/">
                  <i className="fa fa-facebook-official fa-2x" />
                </a>
                <a href="https://www.instagram.com/amcbleckhornen/">
                  <i className="fa fa-instagram fa-2x" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </header>
    )
  }
}
