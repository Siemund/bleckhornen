import React from 'react'

import '../styles/Footer.css'

const Footer = () => {
  return (
    <footer className="clearfix page-footer">
      <div className="container">
        <div className="row">
          <div className="col">
            <p>Made with Fjång! in Lund, &copy;{new Date().getFullYear()}</p>
            <a href="https://www.af.lu.se/">
              <img
                className="logo"
                src="https://res.cloudinary.com/fredriksiemund/image/upload/v1516798298/Bleckhornen/aflogga.png"
                alt="Logo"
              />
            </a>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer
