import React from 'react'

const helpText = (
  <p style={{ fontSize: '12px', marginBottom: '0' }}>
    Fyll i fälten ovan för att skicka formuläret.
  </p>
)

const SendButton = props => {
  switch (props.sending) {
    case true:
      return (
        <div>
          <button className="btn btn-primary">Skickar...</button>
        </div>
      )
    case false:
      return (
        <div>
          <button
            type="submit"
            className={props.canSend ? 'btn btn-primary' : 'btn btn-secondary'}
            disabled={!props.canSend}
          >
            {props.text}
          </button>
          {!props.canSend ? helpText : ''}
        </div>
      )
    default:
      return ''
  }
}

export default SendButton
