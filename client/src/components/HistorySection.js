import React, { Component } from 'react'

export default class HistorySection extends Component {
  render() {
    return (
      <div className="card history-section">
        <a
          data-toggle="collapse"
          href={'#collapse' + this.props.number}
          aria-expanded={this.props.number === 'One' ? 'true' : 'false'}
          aria-controls={'collapse' + this.props.number}
        >
          <div
            className="card-header"
            role="tab"
            id={'heading' + this.props.number}
          >
            <h5 className="mb-0">{this.props.heading}</h5>
          </div>
          <div
            id={'collapse' + this.props.number}
            className={
              this.props.number === 'One' ? 'collapse show' : 'collapse'
            }
            role="tabpanel"
            aria-labelledby={'heading' + this.props.number}
            data-parent="#accordion"
          >
            <div
              className="card-body"
              dangerouslySetInnerHTML={{ __html: this.props.content }}
            />
          </div>
        </a>
      </div>
    )
  }
}
