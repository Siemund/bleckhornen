import React, { Component } from 'react'

export default class CalendarRow extends Component {
  render() {
    return (
      <div className="row cal-row">
        <div className="col-2">
          <div className="card dates-box">
            <div className="card-body dates">
              <p className="text-center">
                <strong>{this.props.weekDay}</strong>
              </p>
              <h5 className="text-center">
                <strong>{this.props.day}</strong>
              </h5>
              <p className="text-center">
                <strong>{this.props.month}</strong>
              </p>
            </div>
          </div>
        </div>
        <div className="col-10">
          <div className="card gig-info-box">
            <div className="card-body gig-info">
              <h5>
                {this.props.time} | {this.props.name}{' '}
              </h5>
              <p>{this.props.address}</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
