import React from 'react'

import GoogleMap from './GoogleMap'
import History from '../containers/History'
import ContactDetails from '../containers/ContactDetails'
import '../styles/About.css'

const About = () => {
  return (
    <div className="about-body">
      <div className="container contact">
        <div className="row">
          <div className="col-xl-8 col-12 google-map">
            <GoogleMap />
          </div>
          <div className="col-xl-4 col-12 contact">
            <h3>Kontakta Bleckhornen</h3>
            <p>
              AcademiMusicCorpset Bleckhornen
              <br />S:t Annegatan 4A
              <br />223 50 Lund
              <br />
              <br />0700-934493
              <br />
              <a href="mailto:info@bleckhornen.org">info@bleckhornen.org</a>
            </p>
            <ContactDetails />
          </div>
        </div>
      </div>

      <div className="container history">
        <History />
      </div>
    </div>
  )
}

export default About
