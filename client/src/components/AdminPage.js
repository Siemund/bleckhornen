import React from 'react'
import UpdateNews from '../containers/UpdateNews'
import UpdateContacts from '../containers/UpdateContacts'

const AdminPage = () => {
  return (
    <div className="container" style={{ paddingTop: 20, paddingBottom: 20 }}>
      <h1>Innehåll på bleckornen.org</h1>
      <UpdateNews />
      <UpdateContacts />
    </div>
  )
}

export default AdminPage
