import React, { Component } from 'react'
import { connect } from 'react-redux'

import HistorySection from '../components/HistorySection'

// import img1 from '../img/urjulkoncert64.jpg'

class History extends Component {
  renderHistorySections() {
    return this.props.history.map(event => {
      return (
        <HistorySection
          number={event.number}
          heading={event.heading}
          content={event.content}
          key={event.number}
        />
      )
    })
  }

  render() {
    return (
      <div id="accordion" role="tablist">
        {this.renderHistorySections()}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    history: state.history
  }
}

export default connect(mapStateToProps)(History)
