import React from 'react'

const UpdateNews = () => {
  return (
    <div className="card" style={{ marginBottom: 20 }}>
      <h5 className="card-header">Senaste nytt</h5>
      <div className="card-body">
        <div class="form-group">
          <input
            type="text"
            class="form-control"
            id="exampleFormControlInput1"
            placeholder="Titel"
          />
        </div>
        <div class="form-group">
          <textarea
            class="form-control"
            id="exampleFormControlTextarea1"
            rows="3"
            placeholder="Innehåll"
          />
        </div>
        <a href="#" className="btn btn-primary">
          Uppdatera
        </a>
      </div>
    </div>
  )
}

export default UpdateNews
