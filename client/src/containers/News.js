import React, { Component } from 'react'
import { connect } from 'react-redux'

class News extends Component {
  render() {
    return (
      <div className="card news-box">
        <h5 className="card-header info-header">Senaste nytt</h5>
        <div className="card-body">
          <h2 className="card-title">{this.props.news[0].title}</h2>
          <p className="card-text">{this.props.news[0].content}</p>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    news: state.news
  }
}

export default connect(mapStateToProps)(News)
