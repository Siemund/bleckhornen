import React, { Component } from 'react'
import { connect } from 'react-redux'
import CalendarRow from '../components/CalendarRow'

class Calendar extends Component {
  renderCalRows() {
    return this.props.gigs.map(event => {
      return (
        <CalendarRow
          weekDay={event.weekDay}
          day={event.day}
          month={event.month}
          time={event.time}
          address={event.address}
          name={event.name}
          key={event.name}
        />
      )
    })
  }

  render() {
    return (
      <div className="card calendar">
        <h5 className="card-header info-header">Här hörs (och syns) vi</h5>
        <div className="card-body">{this.renderCalRows()}</div>
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    gigs: state.gigs
  }
}

export default connect(mapStateToProps)(Calendar)
