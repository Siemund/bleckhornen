import React, { Component } from 'react'
import { connect } from 'react-redux'

import Contact from '../components/Contact'

class ContactDetails extends Component {
  renderContacts() {
    return this.props.contactDetails.map(entry => {
      return (
        <Contact
          post={entry.post}
          name={entry.name}
          mail={entry.mail}
          phone={entry.phone}
          key={entry.phone}
        />
      )
    })
  }

  render() {
    return (
      <table style={{ width: '100%' }}>
        <tbody>{this.renderContacts()}</tbody>
      </table>
    )
  }
}

function mapStateToProps(state) {
  return {
    contactDetails: state.contactDetails
  }
}

export default connect(mapStateToProps)(ContactDetails)
