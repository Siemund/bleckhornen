import React from 'react'

const UpdateContacts = () => {
  return (
    <div className="card">
      <h5 className="card-header">Ansvarsposter</h5>
      <div className="card-body">
        Ordförande:
        <div class="form-group">
          <form>
            <div class="row">
              <div class="col">
                <input type="text" class="form-control" placeholder="Namn" />
              </div>
              <div class="col">
                <input type="email" class="form-control" placeholder="Mail" />
              </div>
              <div class="col">
                <input type="tel" class="form-control" placeholder="Tel." />
              </div>
            </div>
          </form>
        </div>
        <a href="#" className="btn btn-primary">
          Uppdatera
        </a>
      </div>
    </div>
  )
}

export default UpdateContacts
