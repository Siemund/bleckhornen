export default function() {
  return [
    {
      weekDay: 'Torsdag',
      day: '25',
      month: 'Juli',
      time: '15:00',
      name: 'Sommarfest',
      address: 'Grönegatan 5, 22223 Lund'
    },
    {
      weekDay: 'Måndag',
      day: '22',
      month: 'Augusti',
      time: '12:00',
      name: 'Demonstration',
      address: 'Stortorget 1, 22223 Lund'
    },
    {
      weekDay: 'Fredag',
      day: '31',
      month: 'August',
      time: '14:00',
      name: 'Öppet hus',
      address: 'Tegners plats 1, 22223 Lund'
    }
  ]
}
