import { combineReducers } from 'redux'
import GigsReducer from './reducer_gigs'
import HistoryReducer from './reducer_history'
import ContactDetails from './reducer_contacts'
import News from './reducer_news'

//APPLICATION STATE
const rootReducer = combineReducers({
  gigs: GigsReducer,
  history: HistoryReducer,
  contactDetails: ContactDetails,
  news: News
})

export default rootReducer
