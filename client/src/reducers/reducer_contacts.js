export default function() {
  return [
    {
      post: 'Ordförande',
      name: 'Alfred Ahlberg',
      mail: 'ordforande@bleckhornen.org',
      phone: '0731-502290'
    },
    {
      post: 'Vice ordförande',
      name: 'Martin Rogmark',
      mail: 'vice@bleckhornen.org',
      phone: '0732-034547'
    },
    {
      post: 'Kassör',
      name: 'Simon Skåre',
      mail: 'kassor@bleckhornen.org',
      phone: '0704-887200'
    },
    {
      post: 'Sekreterare',
      name: 'Martin Larsson',
      mail: 'sekreterare@bleckhornen.org',
      phone: '0734-165832'
    },
    {
      post: 'Dirigent',
      name: 'Linn Eriksson',
      mail: 'linn94eriksson@gmail.com',
      phone: '0767-990703'
    },
    {
      post: 'Dirigent',
      name: 'Martin Mossberg',
      mail: 'martinmossberg@hotmail.com',
      phone: '0708-213315'
    },
    {
      post: 'Balettledare',
      name: 'Madison Green',
      mail: 'nat15mk2@student.lu.se',
      phone: '0727-133113'
    },
    {
      post: 'Balettledare',
      name: 'Robert Kaneby',
      mail: 'robert.kaneby@hotmail.com',
      phone: '0729-674387'
    }
  ]
}
