export default function() {
  return [
    {
      number: 'One',
      heading: 'Om oss',
      content: `<h3>Lunds bästa och äldsta studentorkester</h3>
            Bleckhornen grundades redan 1956 och sedan dess har vi haft Skåne och världen som vår publik. Hänförda lyssnare 
            i Malmö, Polen och Eslöv har glatt sig åt AcademiMusicCorpset Bleckhornens (till vardags kallat Corpset) glada toner 
            och vita uniformer. Vi känns lätt igen på de vita kläderna och inte att förglömma våra halsdukar och attiraljer, som 
            har den vackra färgkombinationen vita prickar på röd botten.
            <br/>Bleckhornen är inte fakultetsbundet. Alla studerande vid Lunds Universitet är välkomna (om man spelar ett instrument 
            eller dansar, vill säga). Spelning är det i genomsnitt en gång i veckan, mest i Lund, men också i kringliggande omnejd, 
            vilket kan innebära såväl Malmö som Saxtorp, Åhus som Hven. 
            <br/>Bleckhornen disponerar klubblokalen Tarmen med lokaler för styrelse, social samvaro, samlingar och möten. Där inryms 
            även notarkivet, med mer än tusen arrangemang av egna och andras pennor. Den ursprungliga Tarmen låg i AF-borgens 
            källare men 1993 flyttades hela organisationen till källaren i Palais d'Ask och våren 2009 företogs en ny flytt upp 
            till solljuset i AF-borgens fjärde våning.`
    },
    {
      number: 'Two',
      heading: 'Starten',
      content: `<h2>Corpsii grundande och unga år</h2>
            <h5>Födelsen</h5>
            Året var 1956, platsen Korsgatan 4B i centrala Lund. På ovanvåningen av Paulina Dahlbergs enplanshus satt en grupp 
            studenter i trevligt samkväm: herr Lennart Lyngå, Åke Svenstam, Uffe Norsell och Lasse Anderbjörk. Hr Lyngå 
            (sedermera orkesterns medlem #1) hade sedan sommarens eskapader brottats med en djup filosofisk fråga: hur kunde 
            det komma sig att en kulturell metropol som Lund saknade en studentorkester? Just denna afton, måhända inspirerad 
            av sin musikaliskt besläktade bostadsadress, måhända av en mecenatisk musikalisk välvilja gentemot stadens invånare 
            eller måhända av enklare influenser som vin, kvinnor och efterslag, dryftade hr Lyngå sig till att framföra samma 
            undran till sina kamrater. Dessa tog genast till sig idén om att grunda en studentorkester. Så sagt, så gjort.
            <br/>Ett problem dök dock snabbt upp: borde man inte ha något att spela på? Företrädelsevis instrument, men andra orkestrar 
            hade redan vid den tiden bevisat att detta inte var ett absolut krav, särskilt inte med tanke på att detta skulle bli 
            en student-orkester. Men nu ville man ju bli en hornorkester. Alltså begav man sig till Dronningens by och köpte där 
            in en trumpet, två althorn, en tenor och en bas. Alla bättre begagnade, införskaffade för den facila summan av 85 kroner.
            <br/>Orkestern började bygga upp sin musikaliska kompetens och lämnade snart det vid det laget olämpligt benämnda kvarteret 
            "Lugnet" för den måhända mer lämpligt betitlade stadsdelen "Nöden". Här fanns vid denna tid en av stadens privata 
            nattklubbar, Fyrkanten, som blev orkesterns första hem. Två ytterligare medlemmar hade nu anslutit sig, Lennart 
            Malmberg och Bengt Westerberg. Man började repa såväl musikstycken som mod inför alla kulturarbetares stora dröm 
            och oro: att framträda inför publik.
            <br/>Flera frågor uppstod dock. För det första: vad skulle man spela? Repertoaren blev enkel, men effektiv. Bland 
            höjdpunkterna fanns den klassiska marschen Anchor´s Away (Bleckhornen är ju en av ytterst få, om inte den enda, 
            studentorkester som faktisk kan spela marsch) och den då relativt nybakade "Härjarevisan" från spexet Djingis Kahn. 
            För det andra: vilket namn skulle man spela under? Ett antal förslag lades fram, diskuterades och förkastades innan man 
            kom fram till AcademiMusicCorpset Bleckhornen, i vardagstal enkelt kallat Corpset.
            <br/>Orkesterns första spelning blev efter vissa om och men kopplad till Stora Lusse, en dåtida festlighet som inföll vart 
            tredje år. En spänd studentorkester förberedde sig intensivt inför det första publika tontagandet, men glömde nästan en 
            viktig detalj: uniformer! Ett snabbt beslut togs att dessa skulle vara enkla nattskjortor (passande för högtiden om än 
            inte för säsongen), med påbroderad G-klav, för att särskilja musikanterna från andra möjliga stjärngossar.
            <br/>På kvällen den 12/12 1956 avfyrade så Bleckhornen sin första spelning på Akademiska Föreningen. Orkestern fortsatte med 
            att leda den nattliga marsch genom staden som var en del av evenemanget, något som liksom AF-spelningen skulle komma att 
            upprepas ofta ända in i nutiden.
            <br/>
            <br/><h5>Barnsjukdomar</h5>
            Medan orkestern mognade och växte i både numerär och i musikaliskt spann uppstod stridigheter om både det ena och det andra.
            <br/>Först kom stämmostriden, då vissa avantgardistiska element ansåg att man skulle överge den unisona spelstil som dittills 
            rått i orkestern och istället börja spela i stämmor. Majoriteten av medlemmarna ansåg att man följa kutymen i branschen 
            och faktiskt spela flerstämmigt, ett beslut som möjliggjort orkesterns nuvarande musikaliska utsvävningar.
            <br/>Uniformering var ett ständigt besvär under de tidiga åren. Ett tappert försök att inköpa begagnade danska brevbäraruniformer 
            omintetgjordes bestämt av byråkrater i dito land. Istället föll valet på flottans tropikuniform, till vardags benämnd bussarong, 
            om sedermera blev Corpsets standarduniform. Den klarvita, änglalika nyans detta klädesplagg innehar som nytt speglar dock inte 
            nödvändigtvis karaktären hos bäraren av detsamma, inte minst då såväl plagg som bärare tenderar att bli något medtagna, för att 
            inte säga solkiga, efter tillräckligt lång tid i föreningen.
            <br/>Den röd- och vitprickiga kombo som blivit särskilt omhuldad etablerades även den vid denna tid, till viss del av ren slump. 
            Inför Stockholmskarnevalen 1961 uppstod brist på ett av de dittills bärande elementen i uniformeringen, nämligen de blå- och 
            vitprickiga kravatterna. I en kombination av panik, kreativ förstörelse, stundens infall och närmast profetiskt förutseende 
            (då ju rött med vita prickar, till skillnad från sin blånyanserade kusin, skulle visa sig oerhört populärt i designerkretsar) 
            inhandlade #10 Karl-Henning Fredlund en rulle tyg i nämnda nyans som över en natt förvandlades till halsdukar åt en hel orkester.
            <br/>Men det var på den musikaliska fronten det gnisslade mest, inte bara bokstavligt utan även bildligt. Vilken musikalisk art skulle 
            Corpset spela i? Jönsigt eller bayerskt? I typisk Bleckhornsanda blev svaret: både och. Och på samma väg har det fortsatt sen dess, 
            med otaliga kombinationer av högt och lågt, gärna i svårkompatibla musikgenrer och taktarter. Nästa dispyt gällde vem som skulle 
            spela dessa allt mer avancerade tongångar. Klarinetter hade under oklara omständigheter kommit in i orkestern efter ett par år, men 
            1963 beslöt dåvarande styrelse att ta ett drastiskt beslut och införa saxofoner. "Skandal!" skanderade en falang i orkestern, utlös 
            strejk inför en vårkonsert och bildade sedan andra orkestrar, utan träblåsinslag. (En av dessa, Messingorkestern En Tolva Skåne, 
            utvecklades sedermera till ett av landets främsta storband, Tolvan Big Band, då ironiskt nog med en ypperlig saxofon-sektion.) I 
            Bleckhornen blev dock både klarinett- och saxsektionerna etablerade och bärande avdelningar.
            <br/>
            <br/><h5>Framgång redan i ungdomen</h5>
            Det dröjde inte länge förrän Bleckhornen gjorde avtryck på den lundensiska student- och musikhistorien. Redan halvannat år efter 
            grundandet deltog man i 1958 års karneval, då man redan på detta tidiga stadium nådde en delad förstaplats i orkestertävlingen. 
            Corpset har sedan dess varit en given del i karneval och har ofta uppnått mer odelade vinster.
            <br/>Ett par år senare var det dags att introducera ännu en numera fast händelse i Bleckhornskalendern, nämligen vårkoncerten, som avhålls 
            1 maj varje år på Tegnérsplatsen mitt i Lundagård. Denna koncert infördes 1961 delvis som en kupp mot studentsångarna som sedan länge 
            försökt lägga beslag på allt vackert vårväder för egen räkning. Dylika konflikter är dock sedan årtionden tillbaka lösta, och nu råder 
            likvärdig konkurrens om lundabornas musikaliska uppmärksamhet
            <br/>Året därpå, dvs. 1962, släppte Bleckhornen sin första, och hittills största kommersiella hit, den i studentorkestersammanhang ohotade 
            storsäljaren "Patricia". Bakgrunden till framgången var radioprogrammet "20 i topp" där lyssnarna fick skicka in röster till programmet. 
            Corpset insåg snabbt potentialen i det lyckliga sammanträffande att släppandet av nämnda skiva sammanföll inte endast med omröstningen i 
            nämnda program, utan även med Lundakarnevalen 1962. Snart kunde man se Bleckhorn som ivrigt hjälpte firande karnevalister att fylla i 
            röstsedlar. 2200 st fick man ihop, tillräckligt för att hamna i toppen! Om nu inte byråkratin satt käppar i hjulet och rösterna förklarades 
            ogiltiga. Vann gjorde istället Sven-Ingvars med bara lite mer än hälften så många röster. De stora debatterna efter detta debacle ledde 
            delvis till att programmet lades ned, och delvis till att "Patricia" sålde i närmast populärmusikaktiga volymer, samt att Bleckhornen blev 
            riksbekanta. Enbart fördelar, med andra ord.
            <br/>Med dylik vind i seglen och våren redan uppfylld av musikaliska aktiviteter kände orkestern snart ett sug efter händelser även på höstkanten 
            ler varför inte vinterditon? Och varför inte lägga nånting inomhus, det är ju så kallt ute? Och varför inte sitta ner när man nu är inomhus, 
            det blir mycket kortare väg till golvet? Och varför inte klä upp sig lite extra, nu när man inte behövde ha elementtåliga överdrag? Tankarna 
            och planerna tog sakta form och i slutet av november 1964 hölls den första julkoncerten, Fjång. (Den döptes efter den spelstil som alltsedan 
            starten varit ett ledord för föreningen: styrka, attack, uthållighet.) En ny omistlig händelse hade tillfogats den lundensiska kulturkalendern 
            och varje år sedan dess har Corpset lyst upp vintermörkret med både seriös och mindre seriös musik, dans och sång.
            <br/>Mycket har hänt i Corpset sedan dess och många medlemmar har passerat igenom föreningen, men ännu nästan 60 år efter grundandet flödar 
            föreningen av spel- och dansglädje, humor och finlir och är Lunds äldsta och bästa studentorkester.`
    },
    {
      number: 'Three',
      heading: 'Julkoncerter',
      content: `<h2>Julkoncerter</h2>            
            Julkoncerten (märk stavningen) hålls första lördagen i december varje år och höstens arbete inriktas till största del 
            på denna kraftmätning av internationella mått. Två timmar mycket ambitiös och omväxlande underhållning med sång, musik, 
            dans och upptåg i den grandiosare stilen med frack och långklänning och kvällen avslutas med en stor banquette.
            <br/>I en total blandning av humor och allvar tar vi oss an all slags musik på vårt eget sanslösa sätt. Det presenteras många 
            egna arrangemang och ibland även Bleckhornska orginalkompositioner. De danser som framförs är egna tolkningar av verk 
            eller orginalkoreografier av skilda slag, från balett och figurativ marsch till jazz och cabaret. Såväl kostym som dekor 
            står vi för själva.
            <br/>Julkonserten har varje år en gästsolist och under årens lopp har Håkan Hardenberger, Gunilla von Bahr, Anders Paulsson, 
            Lars Holm, Jörgen Ådvall och Lars Wallén varit gäster hos oss.
            <br/>Den första julkoncerten uppfördes 1964, och sedan dess har det varit en stående tradition. 
            <br/>Inom en förhoppningsvis snar framtid kommer Bleckhornens alla julkoncerter listas här, med årtal, namn, gästsolist och 
            bild på affischen från respektive år!`
    }
  ]
}
