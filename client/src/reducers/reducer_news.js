export default function() {
  return [
    {
      title: 'Jubileumsboken är här!',
      content: `With supporting text below as a natural lead-in to additional content.
      Nu är boken också ute i vida världen som E-bok. Jag provköpte den hos
      Bokus för 40:- och den fungerade utmärkt. Googla på books +
      Bleckhornen 60 år så får ni en intressant lista med ställen som boken
      finns på som E-bok, men även den tryckta boken marknadsförs som andra
      länk i listan. Vår bok är utan tvekan årets julklapp! vare sig man
      vill kosta på sig lite mer eller mindre.`
    }
  ]
}
